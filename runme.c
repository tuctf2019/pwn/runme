#include <stdio.h>  // prints
#include <stdlib.h> // malloc
#include <string.h> // strcmp
#include <unistd.h> // read
#include <fcntl.h>  // open
#include <unistd.h> // close
#include <time.h>   // time

int main() {
    setvbuf(stdout, NULL, _IONBF, 20);
    setvbuf(stdin, NULL, _IONBF, 20);

	printf("Enter 'flag'\n> ");
	char buf[32];
	read(0, buf, 32);

	if (!memcmp(buf, "flag", 4))
		puts("TUCTF{7h4nk5_f0r_c0mp371n6._H4v3_fun,_4nd_600d_luck}");
	else
		puts("How did you fail that?");



    return 0;
}
