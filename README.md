# Runme Author Writeup

## Description

Everyone starts somehwere

*Hint:* None

## A Word From The Author

This isn't a real challenge. It's to ensure that people are using a linux environment that has the proper 32bit libraries.

## Information Gathering

run it

## Exploitation

run it

![runit.png](./res/0d6ec4645c634926ba15ca7f4f997713.png)

## Endgame

Did you run it?

> **flag:** TUCTF{7h4nk5_f0r_c0mp371n6._H4v3_fun,_4nd_600d_luck}

